/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_COMMUNICATIONNOISE = 3300250992U;
        static const AkUniqueID PLAY_DRONEMOTOR = 3536391747U;
        static const AkUniqueID PLAY_DRONEROTATION = 1463233682U;
        static const AkUniqueID PLAY_EARTHQUAKE = 3858448649U;
        static const AkUniqueID PLAY_ELECTRIC_MOTOR = 2519717329U;
        static const AkUniqueID PLAY_ELEVATORBELL = 3683180351U;
        static const AkUniqueID PLAY_ELEVATORDOORSCLOSING = 1211901148U;
        static const AkUniqueID PLAY_ELEVATORDOORSCLOSINGTAP = 2240490979U;
        static const AkUniqueID PLAY_ELEVATORDOORSOPENING = 3818758685U;
        static const AkUniqueID PLAY_ELEVATORDRIVE = 2907191916U;
        static const AkUniqueID PLAY_ELEVATOREXTERNALCALLBUTTON = 1835743889U;
        static const AkUniqueID PLAY_ELEVATORINTERNALBUTTON = 3197920139U;
        static const AkUniqueID PLAY_ERRORBEEP = 1304387364U;
        static const AkUniqueID PLAY_ERRORTYPING = 2798153457U;
        static const AkUniqueID PLAY_FAN = 2757069037U;
        static const AkUniqueID PLAY_GARAGEDOORS = 3432241028U;
        static const AkUniqueID PLAY_GENERICBEEP = 644991361U;
        static const AkUniqueID PLAY_INITDRONE = 3653257362U;
        static const AkUniqueID PLAY_LABO_ROOM_TONE = 2983988087U;
        static const AkUniqueID PLAY_LEVER = 3196500978U;
        static const AkUniqueID PLAY_SERVERMOTOR = 2296544690U;
        static const AkUniqueID PLAY_SYNTH_BIPS_SEQUENCE = 3849115581U;
        static const AkUniqueID PLAY_TEMPLE_CHIMES = 1371242783U;
        static const AkUniqueID PLAY_TEMPLE_DRONE = 1164784286U;
        static const AkUniqueID PLAY_TEMPLEINMOLECULATOR_CHIMES = 2321095329U;
        static const AkUniqueID PLAY_TEMPLEINMOLECULATOR_DRONE = 4155344920U;
        static const AkUniqueID PLAY_TORCHLIGHTOFF = 1462694307U;
        static const AkUniqueID PLAY_TORCHLIGHTON = 1588639311U;
        static const AkUniqueID PLAY_TYPING = 1659138091U;
        static const AkUniqueID PLAY_VALIDATIONBEEP = 136370607U;
        static const AkUniqueID STOP_DRONEMOTOR = 2341913985U;
        static const AkUniqueID STOP_DRONEROTATION = 3803467864U;
        static const AkUniqueID STOP_EARTHQUAKE = 3470689151U;
        static const AkUniqueID STOP_ELEVATORDRIVE = 1571487338U;
        static const AkUniqueID STOP_ERRORTYPING = 1200783523U;
        static const AkUniqueID STOP_GARAGEDOORS = 3270412370U;
        static const AkUniqueID STOP_SERVERMOTOR = 1529120824U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID DRONE_SPEED = 1382379429U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID ROOM_TONES = 2183308795U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
